"""
Parses all parquet files in a given directory, and aggregates all column types
in a single dataframe.

In short, for each parquet file, the process reads it, and groups the columns
by their type.
This process is not in charge of doing type inference, only grouping them.
The final dataframe looks like this:

Path                            Numerical columns   Categorical columns
----                            -----------------   -------------------
/path/to/parquet/file1.parquet  col1, col2, col4    col3, col5
/path/to/parquet/file2.parquet  col1, col4          col3

"""

from __future__ import annotations

import random
import logging
import pandas as pd

from pathlib import Path
from typing import Dict, List
from dataclasses import dataclass
from argparse import ArgumentParser

from project import BaseResults, BaseFinalResults
from project.functions import get_files
from project.args import n_jobs_parser, directory_parser, random_parser
from project.parallel import BaseWorker, Pool, setup_logging, result_consumer


@dataclass
class Results(BaseResults):
    file_path: str
    types: Dict[str, List[str]]


@dataclass
class FinalResults(BaseFinalResults):
    read_files: List[str]
    df: pd.DataFrame

    @classmethod
    def from_results(cls, results: List[Results]) -> FinalResults:
        lines = []
        read_files = []
        for result in results:
            read_files.append(result.file_path)
            lines.append((
                result.file_path,
                *[
                    result.types[col]
                    for col in CategoryWorker.groups_assignation
                ]
            ))
        df = pd.DataFrame(
            lines,
            columns=['path'] + list(CategoryWorker.groups_assignation.keys()),
        )
        return cls(read_files, df)


def is_numerical_column(s: pd.Series) -> bool:
    return pd.api.types.is_numeric_dtype(s)


def is_categorical_column(s: pd.Series) -> bool:
    return not pd.api.types.is_numeric_dtype(s)


class CategoryWorker(BaseWorker):

    # Maps group names to a function that classifies whether the passed
    # series (column) corresponds to this category (return a bool).
    # Groups must be mutually exclusive.
    groups_assignation = {
        "Numerical columns": is_numerical_column,
        "Categorical columns": is_categorical_column,
    }

    def group_columns_by_type(self, df: pd.DataFrame) -> Dict[str, List[str]]:
        """
        Given a DataFrame, classify its columns depending on their type
        (as defined by `CategoryWorker.groups_assignation`).
        """
        groups = {
            group_name: list()
            for group_name in self.groups_assignation
        }
        for column_name in df.columns:
            # Because we assume categories (groups) are mutually exclusive,
            # as soon as we found the category for this column,
            # we break the loop and move on to the next column.
            for group_name, categorization_function in self.groups_assignation.items():
                if categorization_function(df[column_name]):
                    groups[group_name].append(column_name)
                    break
        return groups

    def _process(self, file_path: str) -> None:
        df = pd.read_parquet(file_path)
        groups = self.group_columns_by_type(df)
        self.output_queue.put(Results(file_path, groups))


if __name__ == "__main__":
    _parser = ArgumentParser(
        description="See file docstring for description.",
        parents=[directory_parser, random_parser, n_jobs_parser]
    )

    _args = _parser.parse_args()

    setup_logging()
    logger = logging.getLogger()

    # File in which we will store the results
    final_file = Path('./results/column_types.parquet')
    final_file.parent.mkdir(exist_ok=True)

    # Try getting the previous results
    if final_file.exists():
        previous_df = pd.read_parquet(final_file)
    else:
        previous_df = pd.DataFrame(columns=['path'])

    source_directory = str(Path(_args.directory[0]).absolute())

    if _args.random:
        _n_rand = _args.random[0]
        _mode = f'random_{_n_rand}'
        random.seed(0)
    else:
        _mode = 'all'

    logger.info(f'Listing all supported files in {source_directory!r}')
    files = get_files(source_directory, '.parquet', _mode)
    # Filter out files that were already read during a previous execution
    already_read_files = previous_df['path'].to_list()
    files = list(filter(lambda path: path not in already_read_files, files))
    logger.info(f'Got {len(files)} files to process.')

    # Operate the pool
    pool = Pool(
        n_jobs=_args.n_jobs,
        worker_factory=CategoryWorker,
        logger=logger,
    )
    # Add all tasks to the pool
    for file in files:
        pool.put(file)
    pool.start()
    logger.info('Running !')

    final_results = result_consumer(
        logger=logger,
        pool=pool,
        final_results_factory=FinalResults
    )

    # Merge the previously computed data with the new one
    final_df = pd.concat([final_results.df, previous_df], ignore_index=True)
    # And write the final data
    final_df.to_parquet(final_file)

    logger.info(
        f'In total, analyzed {len(final_results.read_files)} files. '
        f'Wrote results to {final_file!s}. '
    )
