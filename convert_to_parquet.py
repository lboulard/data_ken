"""
Given a directory, creates a copy of it where every supported files are
converted to their Apache Parquet equivalent.

File types currently supported:
- CSV

TODO: Fix duplicates when active/passive checking.
"""

from __future__ import annotations

import os
import logging
import pandas as pd

from pathlib import Path
from typing import Dict, List
from dataclasses import dataclass
from argparse import ArgumentParser

from project import BaseResults, BaseFinalResults, Data
from project.config import data_file
from project.database.models import ConvertSchema
from project.functions import try_reading_csv, filter_csv
from project.parallel import BaseWorker, Pool, setup_logging, result_consumer
from project.args import (directory_parser, n_jobs_parser, dry_run_parser,
                          from_zero_parser)


@dataclass
class Results(BaseResults):
    file_path: str
    # Error message we got when trying to read the file.
    # An empty string means no error occurred.
    error: str


@dataclass
class FinalResults(BaseFinalResults):
    errors: Dict[str, str]
    read_files: List[str]

    @classmethod
    def from_results(cls, results: List[Results]) -> FinalResults:
        errors = {}
        read_files = []
        for result in results:
            if result.error:
                errors.update({result.file_path: result.error})
            else:
                read_files.append(result.file_path)
        return cls(errors=errors, read_files=read_files)


class Worker(BaseWorker):

    dry_run: bool
    overwrite: bool
    check_active: bool
    check_passive: bool
    source_directory: Path
    destination_directory: Path

    def deduce_file_destination(self, file_path: Path) -> Path:
        """
        Given the original file path, deduces the location of the destination
        file (in which we will store the parquet version).
        """
        new_file_path = file_path.parent / f'{file_path.stem}.parquet'
        file_path_rel = new_file_path.relative_to(self.source_directory)
        return self.destination_directory / file_path_rel

    def sanity_check(self, df: pd.DataFrame, destination_file: Path) -> str:
        """
        Checks the parquet conversion worked as expected.
        Reads the exported file from disk, and compares the two.
        """
        error = ''

        try:
            on_disk_raw = pd.read_parquet(destination_file, engine='pyarrow')
            on_disk = filter_csv(on_disk_raw)
        except (OSError, TypeError, ValueError) as e:
            error = (f'Sanity check failed for {destination_file!s}. '
                     f'Cannot open file, got error: {e}. ')
            self.logger.error(error)
            return error

        # Init possible errors
        different_shapes = False

        if df.empty and on_disk.empty:
            return error

        if df.shape != on_disk.shape:
            different_shapes = True

        if any([
            different_shapes,
        ]):
            error = f'Sanity check failed for {destination_file!s}. '

        if different_shapes:
            error += f'Different shapes: CSV - {df.shape} ; disk - {on_disk.shape}. '

        if error != '':
            self.logger.error(error)

        return error

    def _process(self, file_path: str) -> None:
        if not os.path.exists(file_path):
            return

        destination_file = self.deduce_file_destination(Path(file_path))
        error = ''

        if not self.check_active and not self.check_passive:
            if destination_file.exists() and not self.overwrite:
                self.logger.debug(f'Skipping {destination_file!s}')
                self.output_queue.put(Results(file_path, error))
                return
        else:
            self.logger.debug(f'Checking {destination_file!s}')

        df_or_error = try_reading_csv(str(file_path))
        if not isinstance(df_or_error, pd.DataFrame):
            error = df_or_error
            self.output_queue.put(Results(file_path, error))
            return

        df = df_or_error  # alias

        if self.dry_run:
            self.logger.info(f'Converted {file_path!s} to '
                             f'{destination_file!s}')
            self.output_queue.put(Results(file_path, error))
            return

        if destination_file.exists():
            if self.check_active or self.check_passive:
                error = self.sanity_check(df, destination_file)
                if error == '':
                    self.logger.debug(
                        f'Sanity check success for {destination_file!r} !'
                    )
                    return
                else:
                    if self.check_active:
                        pass
                    elif self.check_passive:
                        return

        # If the dataset has a single column, skip
        if df.shape[1] == 1:
            error = 'File has only one column.'
            self.output_queue.put(Results(file_path, error))
            return

        destination_file.parent.mkdir(exist_ok=True, parents=True)
        # Fix for https://github.com/pandas-dev/pandas/issues/25043
        df.columns = df.columns.astype(str)
        try:
            df.to_parquet(destination_file, engine='pyarrow')
        except Exception as e:
            error = e
        else:
            self.logger.info(f'Converted {file_path!s} to '
                             f'{destination_file!s}')
            error = self.sanity_check(df, destination_file)

        self.output_queue.put(Results(file_path, error))


if __name__ == "__main__":
    _parser = ArgumentParser(
        description="See file docstring for description.",
        parents=[directory_parser, n_jobs_parser, dry_run_parser,
                 from_zero_parser]
    )
    _parser.add_argument(
        '--overwrite', action='store_true', required=False,
        help="Specify to force overwriting of already existing parquet files. "
             "Most of the time you don't want to do this, and when you do, "
             "you'd likely want to use alongside --from-zero. "
    )
    _parser.add_argument(
        '--process-errors', action='store_true', required=False,
        help="Specify to try to process again the files that had an error "
             "in a previous run. By default, it doesn't."
    )
    _parser.add_argument(
        '--check-passive', action='store_true', required=False,
        help="Specify to force a sanity check on already existing files. "
             "If the check fails, logs an error and does nothing else. "
    )
    _parser.add_argument(
        '--check-active', action='store_true', required=False,
        help="Specify to force a sanity check on already existing files. "
             "If the check fails, the file is converted again. "
    )

    setup_logging()
    logger = logging.getLogger()

    _args = _parser.parse_args()

    Worker.check_passive = _args.check_passive
    Worker.check_active = _args.check_active
    Worker.overwrite = _args.overwrite
    Worker.dry_run = _args.dry
    if _args.dry:
        logger.info('LAUNCHING IN DRY MODE: NOTHING WILL BE MODIFIED')

    # File in which we will store the results
    final_file = Path('./results/convert.parquet')
    final_file.parent.mkdir(exist_ok=True)

    # Try getting the previous results, otherwise init
    if final_file.exists() and not _args.from_zero:
        df = pd.read_parquet(final_file)
    else:
        df = pd.DataFrame(columns=['file', 'error'])

    source_directory = Path(_args.directory[0]).absolute()

    # Extract the destination directory
    Worker.source_directory = source_directory
    new_dir_name = f'{source_directory.name}_parquet'
    destination_directory = source_directory.parent / new_dir_name
    Worker.destination_directory = destination_directory
    if not _args.dry:
        destination_directory.mkdir(exist_ok=True)

    # List files to process
    logger.info(f'Listing all supported files in {source_directory!s}')
    data = pd.read_parquet(data_file)
    _csv_mask = data['path'].str.lower().str.endswith('.csv')
    supported_files = data[_csv_mask]['path']
    # Filter out the files that were read in previous runs
    # (files that are in the table and don't have a value in 'error').
    _error_mask = df['error'] != ''
    if _args.process_errors:
        _previously_read_files = df[~_error_mask]['file']
    else:
        _previously_read_files = df['file']
    _already_read_mask = supported_files.isin(_previously_read_files)
    if _args.check_active or _args.check_passive:
        files = supported_files
    else:
        files = supported_files[~_already_read_mask]

    logger.info(f'Found {len(files)} files to process')

    # Operate the pool
    pool = Pool(
        n_jobs=_args.n_jobs,
        worker_factory=Worker,
        logger=logger,
    )
    # Add the tasks
    logger.info('Adding tasks to the multiprocessing pool')
    for file in files:
        pool.put(file)
    pool.start()
    logger.info('Running !')

    final_results: FinalResults = result_consumer(
        logger=logger,
        pool=pool,
        final_results_factory=FinalResults,
    )

    # Append this run's results
    read_files_df = pd.DataFrame(
        zip(final_results.read_files, [''] * len(final_results.read_files)),
        columns=df.columns,
    )
    errors_df = pd.DataFrame(final_results.errors.items(), columns=df.columns)

    df = pd.concat([df, read_files_df, errors_df], ignore_index=True)

    if not _args.dry:
        # Write the final data
        df.astype(str).to_parquet(final_file)

    logger.info(
        f'This run, converted {len(final_results.read_files)} files out of '
        f'{len(files)} ({_already_read_mask.sum()} in previous runs), '
        f'with {len(final_results.errors)} errors this run '
        f'({_error_mask.sum()} in previous runs). '
        f'Wrote parseable information about this run in {final_file!s}. '
    )
