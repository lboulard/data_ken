import pandas as pd

from collections import Counter


def count_files_by_extension(df: pd.DataFrame) -> Counter:
    def get_ext(s: pd.Series) -> str:
        """
         Given a series, gets the path and returns the extension
         """
        path = s['path']
        parts = path.split('/')
        file_name = parts[-1]
        if '.' in file_name:
            ext = file_name.split('.')[-1]
            return ext.lower()
        else:
            return ''

    exts = Counter(df.apply(get_ext, axis=1))

    # Remove extensions with too few occurrences
    threshold = 10
    for _ext, _occ in exts.copy().items():
        if _occ < threshold:
            exts.pop(_ext)

    return exts
