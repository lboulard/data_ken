import pandas as pd
import plotly.express as px
import plotly.graph_objs as go

from project.config import project_root
from project.plotting import figures_to_html


def scatter_plot(df: pd.DataFrame) -> go.Figure:
    fig = px.scatter(
        df,
        x="n_rows",
        y="n_cols",
        log_x=True,
        log_y=True,
        marginal_x="histogram",
        marginal_y="rug",
        color="size",
        title="Scatter plot of the number of lines/columns per file"
    )
    return fig


if __name__ == "__main__":
    # Load from disk
    df = pd.read_parquet(project_root / 'results' / 'shapes.parquet')

    print(f'Got {df.shape[0]} shapes to display.')

    figures_to_html(
        [
            scatter_plot(df),
        ],
        filename='shapes_scatter.html'
    )
