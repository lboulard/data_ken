"""
Uses the results from the parquet conversion script, and tries to find the
MIME type of the files that had a specific error (error tokenizing data).
If the MIME type does not correspond to the extension, the file is renamed.
"""

from __future__ import annotations

import os
import logging
import pandas as pd

from pathlib import Path
from typing import Tuple, List
from dataclasses import dataclass
from argparse import ArgumentParser

from project import BaseResults, BaseFinalResults
from project.functions import get_mime_type
from project.args import n_jobs_parser, dry_run_parser
from project.parallel import BaseWorker, Pool, setup_logging, result_consumer


@dataclass
class Results(BaseResults):
    file_path: str
    mime_type: str
    did: str


@dataclass
class FinalResults(BaseFinalResults):
    mime_types: List[Tuple[str, str, str]]

    @classmethod
    def from_results(cls, results: List[Results]) -> FinalResults:
        mime_types = []
        for result in results:
            mime_types.append((result.file_path, result.mime_type, result.did))
        return cls(mime_types=mime_types)


class Worker(BaseWorker):

    dry_run: bool

    def _process(self, file_path: str) -> None:
        try:
            mime_type = get_mime_type(file_path)
        except FileNotFoundError:
            self.logger.error(f'Missing file: {file_path!r}')
            return

        if mime_type:
            # We assume file_path ends with '.csv' as this is the only type
            # supported by the conversion script as of April 2022.
            if mime_type != 'application/csv':
                new_ext = mime_type.split('/')[-1]
                if new_ext in {'plain', 'octet-stream'}:
                    did = ''
                else:
                    new_name = f'{file_path}.{new_ext}'
                    if not self.dry_run:
                        os.rename(file_path, new_name)
                    did = 'rename'
                    self.logger.info(
                        f'Renamed {file_path!r} to {new_name!r} because its MIME type '
                        f'did not match its extension (MIME: {mime_type!r}) '
                    )
            else:
                did = ''
            self.output_queue.put(Results(file_path, mime_type, did))
        else:
            self.logger.error(f'Could not determine MIME type of {file_path!r}')


if __name__ == "__main__":
    _parser = ArgumentParser(
        description="See file docstring for description.",
        parents=[n_jobs_parser, dry_run_parser]
    )

    setup_logging()
    logger = logging.getLogger()

    _args = _parser.parse_args()

    Worker.dry_run = _args.dry
    if _args.dry:
        logger.info('LAUNCHING IN DRY MODE: NOTHING WILL BE MODIFIED')

    # File in which we will store the results
    final_file = Path('./results/wrong_type.parquet')
    final_file.parent.mkdir(exist_ok=True)

    # File from which we will get the errors
    convert_file = Path('./results/convert.parquet')
    if not convert_file.exists():
        logger.fatal(f'{convert_file!s} does not exist. Exiting.')
        exit(0)
    conv_df = pd.read_parquet(convert_file)

    # Try getting the previous results, otherwise init
    if final_file.exists():
        df = pd.read_parquet(final_file)
    else:
        df = pd.DataFrame(columns=['file', 'mime_type', 'did'])

    # List files to process
    logger.info('Listing all supported files')
    _valid_errors_mask = conv_df['error'].str.startswith(
        'Error tokenizing data.'
    )
    _valid_files = pd.Series(conv_df[_valid_errors_mask]['file'].unique())
    # Filter out files that were already read.
    _already_read_files = df['file']
    files = _valid_files[~_valid_files.isin(_already_read_files)].to_list()
    logger.info(f'Found {len(files)} files to process.')

    # Operate the pool
    pool = Pool(
        n_jobs=_args.n_jobs,
        worker_factory=Worker,
        logger=logger,
    )
    # Add the tasks
    logger.info('Adding tasks to the multiprocessing pool')
    for file in files:
        pool.put(file)
    pool.start()
    logger.info('Running !')

    final_results: FinalResults = result_consumer(
        logger=logger,
        pool=pool,
        final_results_factory=FinalResults,
    )

    # Append this run's results
    scanned_df = pd.DataFrame(
        final_results.mime_types,
        columns=df.columns,
    )
    df = pd.concat([df, scanned_df], ignore_index=True)

    if not _args.dry:
        # Write the final data
        df.to_parquet(final_file)

    logger.info(
        f'This run, scanned {len(final_results.mime_types)} files out of '
        f'{len(_valid_files)} ({len(_already_read_files)} in previous runs). '
        f'Wrote parseable information about this run in {final_file!s}. '
    )
