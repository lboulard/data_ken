# Data_KEN

Code to preprocess and extract training data for KEN from Maxime Cuny's 
old data located in `/storage/store/data/maxime_cuny/output`.

File `data/files_info.df` was generated with 
[files_to_dataframe](https://github.com/LilianBoulard/utils/tree/main/files_to_dataframe).

# Requirements

Package requirements are listed in the file `requirements.txt`.

They can be installed with

```
pip install -r requirements.txt
```

## Usage

To use the program, use the different scripts under the root directory.

Scripts that create plots are prefixed with `plot_`.

Note that for the program to work, the files listed in the data file need
to be accessible.

If you want to run this script from scratch, re-create the parquet file with
[files_to_dataframe](https://github.com/LilianBoulard/utils/tree/main/files_to_dataframe).
