import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objs as go

from pathlib import Path
from typing import Dict, List, Tuple

from project.plotting import figures_to_html


def ratio_bar_plot(tokens: pd.Series, attributes: pd.Series) -> go.Figure:
    sizes_to_extract: Dict[str, int] = {
        '3K': 3_000,
        '30K': 30_000,
        '300K': 300_000,
        '3M': 3_000_000,
    }
    sizes: List[Tuple[str, str, int]] = []

    total_tokens = tokens.sum()
    total_attributes = attributes.sum()
    current_token_ratio = 0
    current_attribute_ratio = 0
    for name, size in sizes_to_extract.items():
        # Extract ratio for tokens
        token_part = tokens.iloc[:size].sum()
        token_ratio = ((token_part / total_tokens) * 100) - current_token_ratio
        current_token_ratio += token_ratio
        # And for attributes
        attribute_part = attributes.iloc[:size].sum()
        attribute_ratio = ((attribute_part / total_attributes) * 100) - current_attribute_ratio
        current_attribute_ratio += attribute_ratio

        # Add both to the size list.
        sizes.append((name, 'Tokens', token_ratio))
        sizes.append((name, 'Attributes', attribute_ratio))

    df = pd.DataFrame(
        sizes,
        columns=['Most common from 1 to n', 'Type', 'Ratio (%)'],
    )

    fig = px.bar(
        df,
        x="Type",
        y="Ratio (%)",
        color="Most common from 1 to n",
        text="Most common from 1 to n",
        range_y=[0, 100],
        title="Proportion of the most common tokens/attributes on the total"
    )
    return fig


def line_plot(tokens: pd.Series, attributes: pd.Series, samples: int = 100) -> go.Figure:
    points: List[Tuple[int, str, int]] = []

    markers: List[int] = list(
        dict.fromkeys(np.geomspace(1, tokens.shape[0], num=samples, dtype=int))
    )
    total_tokens = tokens.sum()
    total_attributes = attributes.sum()
    for size in markers:
        # Extract ratio for tokens
        token_part = tokens.iloc[:size].sum()
        token_ratio = (token_part / total_tokens) * 100
        # And for attributes
        attribute_part = attributes.iloc[:size].sum()
        attribute_ratio = (attribute_part / total_attributes) * 100
        # Add the two ratios to the point list
        points.append((size, 'Tokens', token_ratio))
        points.append((size, 'Attributes', attribute_ratio))

    df = pd.DataFrame(
        points,
        columns=['Most common from 1 to n', 'Type', 'Ratio (%)'],
    )
    fig = px.line(
        df,
        x='Most common from 1 to n',
        y='Ratio (%)',
        color='Type',
        markers=True,
        range_x=[1, tokens.shape[0]],
        log_x=True,
        title="Proportion of the most common tokens/attributes on the total"
    )
    return fig


if __name__ == "__main__":
    occ = 'occurrences'

    root_dir = Path(__file__).parent.absolute()

    # Load from disk
    _tokens = pd.read_parquet(root_dir / 'results/tokens.parquet', columns=[occ])
    _attributes = pd.read_parquet(root_dir / 'results/attributes.parquet', columns=[occ])

    # Sort them, and store them as series (not dataframes)
    _tokens = _tokens.sort_values(occ, ascending=False, ignore_index=True)[occ]
    _attributes = _attributes.sort_values(occ, ascending=False, ignore_index=True)[occ]

    print(f'{_tokens.shape[0]} tokens and {_attributes.shape[0]} attributes.')

    figures_to_html(
        [
            ratio_bar_plot(_tokens, _attributes),
            line_plot(_tokens, _attributes),
        ],
        filename='ratio.html'
    )
