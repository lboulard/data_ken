"""
Detects and lists files without header.
"""

from __future__ import annotations

import csv
import random
import logging
import pandas as pd

from pathlib import Path
from typing import List, Tuple
from dataclasses import dataclass
from argparse import ArgumentParser

from project import BaseResults, BaseFinalResults
from project.functions import get_files
from project.parallel import BaseWorker, Pool, setup_logging, result_consumer
from project.args import (n_jobs_parser, directory_parser, from_zero_parser,
                          random_parser)


@dataclass
class Results(BaseResults):
    file: str
    has_header: bool


@dataclass
class FinalResults(BaseFinalResults):
    headers: List[Tuple[str, bool]]

    @classmethod
    def from_results(cls, results: List[Results]) -> FinalResults:
        return cls(headers=[
            (result.file, result.has_header) for result in results
        ])


class Worker(BaseWorker):

    def _process(self, file_path: str) -> None:
        """
        Takes a parquet file path as a string, reads it, and uses a sniffer
        to find out whether the file has a header.
        """
        n = 19

        try:
            _df = pd.read_parquet(file_path)
        except Exception as e:
            self.logger.error(f'Got error with {file_path!r}: {e}')
            return

        if _df.empty:
            self.logger.error(f'Could not read {file_path!r} (empty df)')
            return

        lines = _df.iloc[:n]
        has_header = csv.Sniffer().has_header(
            lines.to_csv(quoting=csv.QUOTE_ALL, escapechar='\\'),
        )
        self.output_queue.put(Results(file=file_path, has_header=has_header))


if __name__ == "__main__":
    _parser = ArgumentParser(
        description="See file docstring for description.",
        parents=[directory_parser, n_jobs_parser, from_zero_parser,
                 random_parser]
    )

    _args = _parser.parse_args()

    setup_logging()
    logger = logging.getLogger()

    # File in which we will store the results
    final_file = Path('./results/no_header.parquet')
    final_file.parent.mkdir(exist_ok=True)

    # Try reading the final file, to recover previous results
    if final_file.exists() and not _args.from_zero:
        df = pd.read_parquet(final_file)
    else:
        df = pd.DataFrame(columns=['path', 'has_header'])

    source_directory = str(Path(_args.directory[0]).absolute())

    if _args.random:
        _n_rand = _args.random[0]
        _mode = f'random_{_n_rand}'
        random.seed(0)
    else:
        _mode = 'all'

    logger.info(f'Listing all supported files in {source_directory!r}')
    supported_files = get_files(source_directory, '.parquet', _mode)
    # Filter out files that were already read during a previous execution
    _read_files = df['path'].to_list()
    files = list(filter(lambda path: path not in _read_files, supported_files))
    logger.info(f'Found {len(files)} files to process.')

    # Operate the pool
    pool = Pool(
        n_jobs=_args.n_jobs,
        worker_factory=Worker,
        logger=logger,
    )
    # Add the tasks
    logger.info('Adding tasks to the multiprocessing pool')
    for file in files:
        pool.put(file)
    pool.start()
    logger.info('Running !')

    final_results: FinalResults = result_consumer(
        logger=logger,
        pool=pool,
        final_results_factory=FinalResults
    )

    # Create a dataframe out of this run's data
    run_df = pd.DataFrame(final_results.headers, columns=df.columns)
    # And merge it with the data on disk
    df = pd.concat([df, run_df], ignore_index=True)
    # And write the final data
    df.to_parquet(final_file)

    logger.info(
        f'This run, analyzed {len(final_results.headers)} files '
        f'({len(_read_files)} in previous runs) out of {len(supported_files)}. '
        f'Wrote results to {final_file!s}. '
    )
