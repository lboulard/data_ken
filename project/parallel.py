import sys
import logging
import multiprocessing as mp

from functools import reduce
from abc import abstractmethod
from queue import Empty as EmptyQueue
from typing import List, Callable, Any, Optional, Type
from logging.handlers import QueueHandler, QueueListener

from .utils import get_time
from .classes import BaseFinalResults, ResultsT, FinalResultsT
from .config import logging_format, project_root, default_logging_level


class BaseWorker(mp.Process):

    """
    Generic worker for a multiprocessing task.

    Has an input queue, from which data is pulled, and an output queue,
    to which results are put.

    To implement, subclass `Worker` and overwrite the `_process` method.
    To use it in the queue, pass it at initialization.
    """

    def __init__(self, input_queue: mp.Queue, output_queue: mp.Queue,
                 stop_event: mp.Event, logger):
        super().__init__()
        self._manager = mp.Manager()
        self.input_queue = input_queue
        self.output_queue = output_queue
        self.logger = logger
        self.stop_event = stop_event
        self.done_event = self._manager.Event()

    @abstractmethod
    def _process(self, input_item: Any) -> None:
        """
        Actually do stuff.
        Receives an item from the input queue (`input_item`).
        Inside this function, `put` your results in the output queue
        (`self.output_queue`).
        """
        pass

    def run(self) -> None:
        while not self.stop_event.is_set():
            try:
                input_item = self.input_queue.get(block=False)
            except EmptyQueue:
                break
            else:
                self._process(input_item)
        self.done_event.set()


class Pool:

    def __init__(self, n_jobs: int, worker_factory, logger):
        self._manager = mp.Manager()
        # Queue in which we will put the files
        self.input_queue = self._manager.Queue()
        # Queue in which each worker will put its results, and a consumer
        # will have to process them.
        self.output_queue = self._manager.Queue()
        # Event used to indicate that the process must be stopped,
        # even if unfinished.
        self.stop_event = self._manager.Event()
        self.logger = logger
        self._workers = [
            worker_factory(
                self.input_queue, self.output_queue,
                self.stop_event, self.logger
            )
            for _ in range(n_jobs)
        ]

    @property
    def n_jobs(self) -> int:
        return len(self._workers)

    def is_done(self) -> bool:
        """
        Whether the pool has finished all the work.
        Will return False if not yet launched.
        Independent of `join` or `stop`.
        """
        # Will perform a logical `and` on all the worker statuses,
        # meaning that if they all are done, this method returns True.
        return reduce(
            lambda acc, status: acc and status,
            self._map(lambda worker: worker.done_event.is_set()),
            True,
        )

    def _map(self, callback: Callable[[BaseWorker], Any]) -> List[Any]:
        """
        Apply a callback to all the workers in this pool.
        The callback takes exactly one argument: a worker.
        """
        return [callback(worker) for worker in self._workers]

    def empty_unprocessed(self):
        """
        Return the content of the input queue, emptying it.
        These represent tasks we have not processed.
        """
        return [
            self.input_queue.get()
            for _ in range(self.input_queue.qsize())
        ]

    def put(self, task: Any) -> None:
        """
        Adds a task to process.
        """
        self.input_queue.put(task)

    def get(self, block: bool = True, timeout: Optional[int] = None):
        """
        Gets a result.
        """
        return self.output_queue.get(block=block, timeout=timeout)

    def start(self) -> None:
        """
        Start all the processes. Returns immediately (is non-blocking).
        """
        self._map(lambda worker: worker.start())

    def join(self) -> None:
        """
        Wait for the workers to finish. Is blocking.
        """
        self._map(lambda worker: worker.join())

    def close(self) -> None:
        """
        Close workers.
        """
        self._map(lambda worker: worker.close())

    def stop(self) -> None:
        """
        Gracefully stop the workers. Is blocking.
        """
        self.stop_event.set()
        self.join()
        self.close()

    def kill(self) -> None:
        """
        Interrupts all workers.
        """
        self._map(lambda worker: worker.kill())


def setup_logging() -> None:
    """
    Ugly function used to set up logging. To be called only once (at startup).
    A cleaner way would've been to use a config file, but I didn't find a way
    of integrating a QueueHandler, as it needs a queue object.
    """
    log_queue = mp.Queue()

    _logs_dir = project_root / 'logs'
    _logs_dir.mkdir(exist_ok=True)
    log_file = _logs_dir / f'data_ken_{get_time()}.log'

    standard_formatter = logging.Formatter(fmt=logging_format)

    console_handler = logging.StreamHandler(sys.stdout)
    file_handler = logging.FileHandler(log_file)
    queue_handler = QueueHandler(log_queue)

    console_handler.setFormatter(standard_formatter)
    file_handler.setFormatter(standard_formatter)
    # For the queue handler, we'll print the message directly, as it will be
    # formatted by the sub-handlers.
    queue_handler.setFormatter(logging.Formatter(fmt='%(message)s'))

    # Configure QueueListener.
    # As we use multiprocessing, this listener will be in charge of receiving
    # the logs send by the different processes (via the log queue), and write
    # them in a file.
    _log_listener = QueueListener(
        log_queue,
        console_handler, file_handler,
        respect_handler_level=True
    )

    # Configure logger
    _logger = logging.getLogger()
    _logger.addHandler(queue_handler)
    _logger.setLevel(default_logging_level)
    _logger.propagate = False

    _log_listener.start()


def result_consumer(logger, pool: Pool,
                    final_results_factory: Type[BaseFinalResults]
                    ) -> FinalResultsT:
    """
    Listens for results sent by the workers, and processes them along the way.
    Handles keyboard interrupts and exceptions.
    After the function is done, the pool is stopped and closed,
    and the final results are returned.
    """
    results: List[ResultsT] = []
    try:
        while not pool.is_done():
            try:
                result: ResultsT = pool.get(timeout=2)
            except (EmptyQueue, TimeoutError):
                continue
            else:
                results.append(result)
        pool.stop()
    except KeyboardInterrupt:
        logger.info('Got KeyboardInterrupt, stopping gracefully.')
        pool.kill()
        pass
    except Exception as e:
        logger.fatal('Got fatal error during processing:')
        logger.exception(e)
        pool.kill()
    finally:
        return final_results_factory.from_results(results)
