from typing import List
import plotly.graph_objs as go


def figures_to_html(figs: List[go.Figure], filename: str):
    """
    From https://stackoverflow.com/a/58336718/9084059
    and adapted to include plotlyjs only once
    """
    with open(filename, 'w') as dashboard:
        dashboard.write("<html><head></head><body>" + "\n")
        got_plotlyjs = False
        for fig in figs:
            inner_html = fig.to_html(
                include_plotlyjs=not got_plotlyjs
            ).split('<body>')[1].split('</body>')[0]
            got_plotlyjs = True
            dashboard.write(inner_html)
        dashboard.write("</body></html>" + "\n")
