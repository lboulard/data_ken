"""
Defines argument parsers with generic arguments.

Use by specifying these parsers as parents in your ArgumentParser.
"""

from argparse import ArgumentParser


n_jobs_parser = ArgumentParser(add_help=False)
n_jobs_parser.add_argument(
    '--n_jobs', required=False, default=4, type=int,
    help="How many processes we want to create. 4 by default. "
)

directory_parser = ArgumentParser(add_help=False)
directory_parser.add_argument(
    '-d', '--directory', required=True, nargs=1, type=str,
    help="Directory to explore. "
)

random_parser = ArgumentParser(add_help=False)
random_parser.add_argument(
    '--random', required=False, nargs=1, type=int,
    help="Instead of reading all files, read a random subset. "
)

from_zero_parser = ArgumentParser(add_help=False)
from_zero_parser.add_argument(
    '--from-zero', action="store_true", required=False,
    help="If specified, scraps the data extracted from previous runs, "
         "and runs the program from zero.",
)

dry_run_parser = ArgumentParser(add_help=False)
dry_run_parser.add_argument(
    '--dry', required=False, action='store_true',
    help="Launch in dry mode: does all the computation but does not "
         "modify information on disk. "
)
