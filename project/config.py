import logging

from pathlib import Path


project_root: Path = Path(__file__).parent

data_file: Path = project_root.parent / 'data' / 'files_info.parquet'
attributes_file: Path = project_root.parent / 'attributes.parquet'
tokens_file: Path = project_root.parent / 'tokens.parquet'
default_logging_level: int = logging.INFO
logging_format: str = '%(asctime)s - [%(levelname)s] [%(process)s] %(message)s'
