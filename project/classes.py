from __future__ import annotations

import pandas as pd

from collections import Counter
from dataclasses import dataclass
from abc import ABC, abstractmethod
from typing import Union, List, TypeVar


class RotatingCounter:

    def __init__(self, max_tokens: int = 10000):
        super().__init__()
        self.max_tokens = max_tokens
        self.counter = Counter()

    def __len__(self):
        return len(self.counter)

    @classmethod
    def from_df(cls, df: pd.DataFrame):
        """
        Takes a DataFrame formatting a RotatingCounter (as exported by `to_df`)
        and returns an instance of RotatingCounter.
        """
        counter = cls()
        for index, series in df.iterrows():
            counter.update({series['token']: series['occurrences']})
        return counter

    def update(self, d) -> None:
        """
        Same as `Counter.update`
        """
        if isinstance(d, RotatingCounter):
            self.counter.update(d.counter)
        else:
            self.counter.update(d)

    def items(self):
        """
        Same as `Counter.items`
        """
        return self.counter.items()

    def merge(self, counter: Union[Counter, RotatingCounter]):
        """
        Given another counter, merge its contents into this instance.
        """
        self.update(counter)

    def to_df(self) -> pd.DataFrame:
        """
        Converts this instance to a pandas DataFrame.
        """
        return pd.DataFrame(self.items(), columns=('token', 'occurrences'))


@dataclass
class BaseResults(ABC):
    """
    Object representing the results returned by a process from the pool.
    """
    pass


# Type used for denoting an instance of a derived class of BaseResults.
ResultsT = TypeVar('ResultsT', bound=BaseResults)


@dataclass
class BaseFinalResults(ABC):
    @classmethod
    @abstractmethod
    def from_results(cls, results: List[ResultsT]):
        """
        Takes a list of results and returns the final results,
        which is what the pool will return.
        """


# Type used for denoting an instance of a derived class of BaseFinalResults.
FinalResultsT = TypeVar('FinalResultsT', bound=BaseFinalResults)
