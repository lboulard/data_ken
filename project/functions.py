import os
import magic
import random
import pandas as pd

from csv import Sniffer, Dialect, Error as DialectError
from typing import Optional, Iterator, Callable, List, Union

from .utils import get_encoding


def guess_csv_dialect(file_path: str,
                      encoding: str = "utf8",
                      max_confidence: bool = False) -> Dialect:
    """
    Guess CSV file dialect by sniffing the first few rows and return it.
    If `max_confidence` is True, will pass the whole file to the sniffer,
    which, as you can imagine, is memory heavy and might take a while.
    """
    with open(file_path, mode="r", encoding=encoding) as f:
        if max_confidence:
            lines = f.read()
        else:
            lines = "".join([f.readline() for _ in range(10)])
    dialect = Sniffer().sniff(lines)
    return dialect


def filter_csv(df: pd.DataFrame) -> pd.DataFrame:
    df = drop_dirty_columns(df)
    return df


def try_reading_csv(file_path: str) -> Union[pd.DataFrame, str]:
    """
    Given a CSV file path, tries to read it.
    It will pick from a list of possible encodings and dialects,
    making more educated guesses on both at each iteration.
    Returns a DataFrame if it could read it, a string describing the error
    if there was any.
    """

    encoding_guess_funcs: Iterator[Callable[[], str]] = iter([
        lambda: get_encoding(file_path, max_confidence=False),
        lambda: get_encoding(file_path, max_confidence=True),
    ])
    csv_dialect_guess_funcs: Iterator[Callable[[], Dialect]] = iter([
        lambda: guess_csv_dialect(file_path, encoding=encoding_guess,
                                  max_confidence=False),
        lambda: guess_csv_dialect(file_path, encoding=encoding_guess,
                                  max_confidence=True),
    ])

    error: str = ''

    encoding_guess: str = "utf8"  # Default
    dialect_guess: Optional[Dialect] = None  # Default
    got_encoding_best_guess: bool = False
    got_dialect_best_guess: bool = False
    while not got_dialect_best_guess and not got_encoding_best_guess:
        try:
            df = pd.read_csv(
                file_path,
                encoding=encoding_guess,
                dialect=dialect_guess,
            )
            assert df.shape[1] > 1, (
                "Got only one column, which might be an indication "
                "that the guessed dialect (delimiter) is incorrect."
            )
        except UnicodeDecodeError as e:
            error = str(e)
            try:
                encoding_guess = next(encoding_guess_funcs)()
                assert encoding_guess is not None
            except AssertionError:
                # The encoding guess can return None, so if it does,
                # we skip to the next iteration.
                continue
            except StopIteration:
                got_encoding_best_guess = True
        except (pd.errors.ParserError, AssertionError) as e:
            error = str(e)
            try:
                dialect_guess = next(csv_dialect_guess_funcs)()
            except StopIteration:
                got_dialect_best_guess = True
            except DialectError as e:
                error = str(e)
            except UnicodeDecodeError as e:
                error = str(e)
                try:
                    encoding_guess = next(encoding_guess_funcs)()
                    assert encoding_guess is not None
                except AssertionError:
                    # The encoding guess can return None, so if it does,
                    # we skip to the next iteration.
                    continue
                except StopIteration:
                    got_encoding_best_guess = True
        except Exception as e:
            error = str(e)
            break
        else:
            return filter_csv(df)
    return error


def drop_dirty_columns(df: pd.DataFrame) -> pd.DataFrame:
    # Remove numerical columns
    df = df.select_dtypes(exclude=["number", "datetime"])
    df = df.astype(str)
    for col in df.columns:
        token_lengths = df[col].str.len()
        # Exclude columns with too much text
        if token_lengths.max() > 100:
            del df[col]
        # else: # Exclude columns with too many numbers
        #     n_digits = df[col].str.count("\d")
        #     if n_digits / entity_lengths > 0.75
    return df


def extract_attributes(df: pd.DataFrame) -> List[str]:
    """
    Takes a DataFrame, and returns the column names.
    """
    # Remove numerical columns
    df = df.select_dtypes(exclude=["number", "datetime"])
    return list(df.columns)


def extract_tokens(df: pd.DataFrame) -> List[str]:
    """
    Takes a DataFrame, and returns the list of all the tokens it contains.
    """
    # Remove numerical columns
    df = df.select_dtypes(exclude=["number", "datetime"])
    return df.values.flatten().tolist()


def get_mime_type(file: str) -> Optional[str]:
    """
    Given a file, guess its MIME type.
    """
    return magic.from_file(file, mime=True)


def get_files(directory: str, ext: str, mode: str) -> List[str]:
    """
    Recursively explores a directory, and returns the list of files with a
    certain extension.
    `mode` can take two values:
    - 'all' ; returns all the files encountered
    - 'random_X' ; returns a random subset of length X. E.g., 'random_100'.
    """
    files: List[str] = []
    for dirpath, dirnames, filenames in os.walk(directory):
        files.extend([
            os.path.join(dirpath, filename)
            for filename in filenames
            if filename.lower().endswith(ext)
        ])
    if mode == 'all':
        return files
    else:
        _, n = mode.split('_')
        return random.sample(files, int(n))
