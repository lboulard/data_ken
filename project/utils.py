import magic
import chardet

from copy import copy
from time import time
from math import floor
from io import BufferedIOBase
from typing import Union, Optional, BinaryIO


def human_readable_size(size: int) -> str:
    """
    Takes a size as bytes and returns a human-readable version.
    """
    original_size = copy(size)
    units = ["B", "KB", "MB", "GB", "TB"]
    unit_index = 0
    while size >= 1024:
        size /= 1024
        unit_index += 1
    u = units[unit_index]
    size = f"{int(size)} {u} ({original_size} B)"
    return size


def get_mime_type(file: Union[str, BinaryIO]) -> str:
    """
    Given a file, guess its MIME type.
    """
    if isinstance(file, str):
        fl = open(file, mode='rb')  # Don't forget to close !
    elif issubclass(file.__class__, BufferedIOBase):
        # If the buffer has been opened outside this function,
        # we won't close it here.
        fl = file
    else:
        raise ValueError(
            f'Excepted "file" to be a string or a buffer, got {type(file)!r}!'
        )

    mime_type = magic.from_buffer(fl, mime=True)

    if isinstance(file, str):
        fl.close()

    return mime_type


def get_encoding(file: Union[str, BinaryIO],
                 max_confidence: bool = False) -> Optional[str]:
    """
    Given a file, guess its encoding.
    Returns an encoding supported by chardet,
    see https://chardet.readthedocs.io/en/latest/supported-encodings.html.
    If "max_confidence" is True (default False), the file will be read entirely
    to have the maximum confidence possible. As you can imagine, doing so
    is very slow.
    If the process couldn't find an appropriate encoding, returns None.
    """
    detector = chardet.UniversalDetector()  # Cannot use `with` unfortunately

    if isinstance(file, str):
        fl = open(file, mode='rb')  # Don't forget to close !
    elif issubclass(file.__class__, BufferedIOBase):
        # If the buffer has been opened outside of this function,
        # we won't close it here.
        fl = file
    else:
        raise ValueError(
            f'Excepted "file" to be a string or a buffer, got {type(file)!r}!'
        )

    if max_confidence:
        return chardet.detect(fl.read())['encoding']
    else:
        while True:
            # Read the file chunk by chunk
            chunk = fl.read(1024 * 32)
            if not chunk:
                # We arrived at the end of the file without finding
                # an appropriate encoding.
                detector.close()
                return

            # And feed the detector along the way.
            detector.feed(chunk)

            # Once it is confident enough about the encoding prediction,
            # end reading and return the guessed encoding.
            if detector.done:
                detector.close()
                break

    if isinstance(file, str):
        fl.close()

    return detector.result['encoding']


def get_time(*, offset: int = 0) -> int:
    """
    Returns the current time as a Unix timestamp.
    If `offset` (another Unix timestamp) is passed,
    returns the time elapsed between the two.
    """
    return floor(time() - offset)
