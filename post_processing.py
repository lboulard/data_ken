"""
Applies some post-processing steps to better format the parquet files.

This currently includes:
- Replacing ambiguous values for denoting missing data (e.g. "nan", "?", etc)
  and actually converting them in an appropriate type (`pd.NA` or `np.nan`).
- Inferring data type per column

The datasets are post-processed and overwritten in-place on disk.

An output file is written in the current directory, containing parsable
information about the run, so that following runs can build upon
the previous ones.
"""

from __future__ import annotations

import random
import logging
import pandas as pd

from pathlib import Path
from typing import Dict, List
from dataclasses import dataclass
from argparse import ArgumentParser
from dirty_cat import SuperVectorizer

from project import BaseResults, BaseFinalResults
from project.utils import get_time
from project.functions import get_files
from project.args import directory_parser, n_jobs_parser, random_parser
from project.parallel import BaseWorker, Pool, setup_logging, result_consumer


@dataclass
class Results(BaseResults):
    file_path: str
    error: str
    timestamp: int


@dataclass
class FinalResults(BaseFinalResults):
    processed_files: Dict[str, int]
    errors: Dict[str, str]

    @classmethod
    def from_results(cls, results: List[Results]) -> FinalResults:
        processed_files = {}
        errors = {}
        for result in results:
            if result.error:
                errors.update({result.file_path: result.error})
            else:
                read_files.append(result.file_path)
        return cls(processed_files, errors)


class Worker(BaseWorker):

    def _process(self, file_path: str) -> None:
        df = pd.read_parquet(file_path)
        error = ''
        # Normalize missing values
        df = df.replace()
        # Cast the columns to their best possible type
        df = SuperVectorizer._auto_cast(df)
        # Overwrite the original DataFrame
        df.to_parquet(file_path)
        self.output_queue.put(Results(file_path, error, get_time()))


if __name__ == "__main__":
    _parser = ArgumentParser(
        description="See file docstring for description.",
        parents=[directory_parser, random_parser, n_jobs_parser]
    )
    _parser.add_argument(
        '--last_update', nargs=1, type=int, required=False,
        help="Specifies the last time the post-processing code was updated, "
             "so that files before this timestamp can be re-processed. "
    )

    _args = _parser.parse_args()

    setup_logging()
    logger = logging.getLogger()

    # File in which we will store the results
    final_file = Path('./results/post_proc_results.parquet')
    final_file.parent.mkdir(exist_ok=True)

    # Try getting the previous results, otherwise init
    if final_file.exists():
        previous_df = pd.read_parquet(final_file)
    else:
        previous_df = pd.DataFrame(columns=['path', 'last_processed'])

    source_directory = str(Path(_args.directory[0]).absolute())

    if _args.random:
        _n_rand = _args.random[0]
        _mode = f'random_{_n_rand}'
        random.seed(0)
    else:
        _mode = 'all'

    logger.info(f'Listing all supported files in {source_directory!r}')
    files = get_files(source_directory, '.parquet', _mode)
    # Filter out files that were processed after last update
    read_files = previous_df['path'].to_list()
    files = list(filter(lambda path: path not in read_files, files))
    logger.info(f'Got {len(files)} files to process.')

    # Operate the pool
    pool = Pool(
        n_jobs=_args.n_jobs,
        worker_factory=Worker,
        logger=logger,
    )
    # Add all tasks to the pool
    for file in files:
        pool.put(file)
    pool.start()
    logger.info('Running !')

    final_results = result_consumer(
        logger=logger,
        pool=pool,
        final_results_factory=FinalResults
    )

    logger.info(
        f'Post-processed {len(final_results.read_files)} files out of '
        f'{len(files)}, with {len(final_results.errors)} errors this run. '
    )
