import pandas as pd

from collections import Counter


def str_tuple_to_list(str_tuple: str, lower: bool = True) -> list:
    parts = str_tuple[1:-1].split(', ')
    if lower:
        parts = [part.lower() for part in parts]
    return parts


df = pd.read_csv('results/attribute_search.csv')

for entity in df['attribute'].unique():
    # Get the lines with only the attributes this loop is interested in
    sub_df = df[df['attribute'] == entity]
    # Filter out lines with a low cardinality (<100)
    sub_df = sub_df[sub_df['cardinality'] > 100]
    # Only keep columns that match exactly (case-insensitive)
    sub_df = sub_df[sub_df['column_name'].str.lower() == entity]
    # Sort values, the highest cardinality first
    sub_df = sub_df.sort_values(by='cardinality', ascending=False)
    # Count attributes that co-occur
    counter = Counter()
    for attributes in sub_df['other_cols'].to_list():
        counter.update(str_tuple_to_list(attributes))
    print(f"Frequently co-occurring attributes for {entity!r}: ")
    print(counter.most_common(25))
