import pandas as pd

from copy import copy
from collections import defaultdict

from project.utils import human_readable_size


def count_size_by_extension(data: pd.DataFrame) -> dict:

    def add_size(s: pd.Series) -> None:
        """
        Given a series, gets the path and returns the extension
        """
        path = s['path']
        parts = path.split('/')
        file_name = parts[-1]
        if '.' in file_name:
            ext = file_name.split('.')[-1].lower()
        else:
            ext = ''
        size = s['size']
        sizes[ext] += size

    sizes = defaultdict(lambda: 0)
    data.apply(add_size, axis=1)
    # Remove extensions with too low usage
    threshold = 100000  # 100 MB
    for _ext, _size in copy(sizes).items():
        if _size < threshold:
            sizes.pop(_ext)
    # Sort the extensions
    sizes = dict(sorted(sizes.items(), key=lambda pair: pair[1], reverse=True))
    # Convert sizes to human-readable values
    sizes = {
        _key: human_readable_size(_size)
        for _key, _size in sizes.items()
    }

    return sizes
