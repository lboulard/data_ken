"""
Extracts the shapes of all the CSV files it finds.
Writes a parquet file with the following columns: path, error, n_rows, n_cols
If "error" is not empty, shapes are missing.
"""

from __future__ import annotations

import os
import logging
import pandas as pd

from pathlib import Path
from dataclasses import dataclass
from argparse import ArgumentParser
from typing import List, Tuple, Optional

from project.config import data_file
from project.utils import human_readable_size
from project.functions import try_reading_csv
from project import BaseResults, BaseFinalResults
from project.args import n_jobs_parser, from_zero_parser
from project.parallel import Pool, BaseWorker, setup_logging, result_consumer


@dataclass
class Results(BaseResults):
    file_path: str
    error: str
    shape: Tuple[Optional[int], Optional[int]]
    size: Optional[int]


@dataclass
class FinalResults(BaseFinalResults):
    df: pd.DataFrame

    @classmethod
    def from_results(cls, results: List[Results]) -> FinalResults:
        files = []
        errors = []
        rows = []
        columns = []
        sizes = []

        for result in results:
            files.append(result.file_path)
            errors.append(result.error)
            n_rows, n_cols = result.shape
            rows.append(n_rows)
            columns.append(n_cols)
            sizes.append(result.size)

        return cls(
            pd.DataFrame(zip(files, errors, rows, columns, sizes),
                         columns=['path', 'error', 'n_rows', 'n_cols', 'size'])
        )


class Worker(BaseWorker):

    def _process(self, csv_file: str) -> None:
        error = ''

        try:
            size = os.stat(csv_file).st_size
            file_size = human_readable_size(size)
        except FileNotFoundError:
            file_size = error = 'Not found'
        self.logger.info(f'Processing {csv_file} ({file_size})')

        if error != '':
            self.output_queue.put(Results(
                file_path=csv_file,
                error=error,
                shape=(None, None),
                size=None,
            ))
            return

        df_or_error = try_reading_csv(csv_file)

        if isinstance(df_or_error, pd.DataFrame):
            df = df_or_error  # alias
            shape = df.shape
        else:
            error = df_or_error
            shape = (None, None)

        self.output_queue.put(Results(
            file_path=csv_file,
            error=error,
            shape=shape,
            size=size,
        ))


if __name__ == "__main__":

    _parser = ArgumentParser(
        description="See file docstring for description.",
        parents=[n_jobs_parser, from_zero_parser]
    )

    _args = _parser.parse_args()

    setup_logging()
    logger = logging.getLogger()

    # File in which we will store the results
    final_file = Path('./results/shapes.parquet')
    final_file.parent.mkdir(exist_ok=True)

    # Try getting the previous results, otherwise init
    if final_file.exists() and not _args.from_zero:
        logger.info('Loading previous results')
        df = pd.read_parquet(final_file)
        logger.info(f'Found {df.shape[0]} previously read files')
    else:
        logger.info('Starting from zero')
        df = pd.DataFrame(columns=['path', 'error', 'n_rows', 'n_cols', 'size'])

    # List files to process
    logger.info(f'Listing all supported files')
    data = pd.read_parquet(data_file)
    _csv_mask = data['path'].str.lower().str.endswith('.csv')
    supported_files = pd.Series(data[_csv_mask]['path'].unique())
    _previously_read_files = df['path']
    _already_read_mask = supported_files.isin(_previously_read_files)
    files = supported_files[~_already_read_mask]

    logger.info(f'Found {len(files)} files to process')

    # Operate the pool
    pool = Pool(
        n_jobs=_args.n_jobs,
        worker_factory=Worker,
        logger=logger,
    )
    # Add the tasks
    logger.info('Adding tasks to the multiprocessing pool')
    for file in files:
        pool.put(file)
    pool.start()
    logger.info('Running !')

    final_results: FinalResults = result_consumer(
        logger=logger,
        pool=pool,
        final_results_factory=FinalResults,
    )

    logger.info(f'Final results: {final_results.df.shape}')
    # Append this run's results
    df = pd.concat([df, final_results.df], ignore_index=True)

    # Write the final data
    df.to_parquet(final_file)

    logger.info(
        f"Extracted shapes of {df.shape[0]} files out of {len(supported_files)}"
    )
