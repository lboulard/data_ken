"""
Given an attribute (case-insensitive), searches for it in all the files of the
parquet mirror and compiles the results in a parseable format.

Final file looks like this:
entity                   path        column_name  cardinality  n_rows                                     head             other_cols
  city  /path/to/file.parquet  City of residence         1000   90000  (Paris, Palaiseau, Massy, Saclay, Rome)  (Population, Surface)

"""

from __future__ import annotations

import logging
import pandas as pd
import pyarrow as pa

from typing import List
from pathlib import Path
from argparse import ArgumentParser
from pyarrow.parquet import ParquetFile
from dataclasses import dataclass, astuple

from project import BaseResults, BaseFinalResults
from project.args import n_jobs_parser, from_zero_parser
from project.parallel import BaseWorker, Pool, setup_logging, result_consumer


@dataclass
class Results(BaseResults):
    attribute: str
    path: str
    column_name: str
    cardinality: int
    n_rows: int
    head: tuple
    other_cols: tuple


@dataclass
class FinalResults(BaseFinalResults):
    df: pd.DataFrame

    @classmethod
    def from_results(cls, results: List[Results]) -> FinalResults:
        return cls(
            pd.DataFrame(
                [astuple(result) for result in results],
                columns=Results.__dataclass_fields__.keys(),
            ),
        )


class Worker(BaseWorker):

    # The attribute to search for.
    attribute: str
    # Number of values to be put in the 'head' field.
    head_size: int

    def _process(self, file_path: str) -> None:
        # First, read only the first line to get the header
        try:
            pf = ParquetFile(file_path)
        except (OSError, pa.lib.ArrowInvalid) as e:
            self.logger.error(f"Can't read {file_path!r}: {e}")
            return
        try:
            one_row = next(pf.iter_batches(batch_size=1))
        except StopIteration:
            # File doesn't have any lines.
            self.logger.error(f"Could not read first line of {file_path!r} because it is empty")
            return
        # noinspection PyArgumentList
        first_line: pd.DataFrame = pa.Table.from_batches([one_row]).to_pandas()

        # List matching columns
        matching_columns: List[str] = []
        for col in first_line.columns:
            if self.attribute in col.lower():
                matching_columns.append(col)

        # If the list is empty, stop
        if not matching_columns:
            return

        df = pd.read_parquet(file_path, columns=matching_columns)
        for col in matching_columns:
            self.output_queue.put(
                Results(
                    attribute=self.attribute,
                    path=file_path,
                    column_name=str(col),
                    cardinality=df[col].unique().size,
                    n_rows=df[col].shape[0],
                    head=tuple(df.head(self.head_size)[col].astype(str).to_list()),
                    other_cols=tuple(first_line.columns.astype(str).to_list()),
                )
            )


if __name__ == "__main__":
    _parser = ArgumentParser(
        description="See file docstring for description.",
        parents=[n_jobs_parser, from_zero_parser]
    )
    _parser.add_argument('-a', '--attribute', type=str, nargs=1, required=True,
                         help="Attribute to search for. Case-insensitive. ")
    _parser.add_argument('--head-size', type=int, nargs=1, required=False,
                         default=[5],
                         help="Number of elements to be collected for the "
                              "'head' field. Not retroactive. Default is 5. ")
    _parser.add_argument('-f', '--force', action='store_true', required=False,
                         help="Force scanning if attribute is already "
                              "in the results. "
                              "The already existing lines will be removed. "
                              "By default, the program will exit. ")

    _args = _parser.parse_args()

    setup_logging()
    logger = logging.getLogger()

    # File in which we will store the results
    final_file = Path('./results/attribute_search.csv')
    final_file.parent.mkdir(exist_ok=True)

    # Try getting the previous results, otherwise init
    if final_file.exists() and not _args.from_zero:
        df = pd.read_csv(final_file)
    else:
        df = pd.DataFrame(columns=Results.__dataclass_fields__.keys())

    Worker.attribute = _args.attribute[0]
    Worker.head_size = _args.head_size[0]

    logger.info('Reading data file')
    data = pd.read_parquet('data/files_info.parquet')
    logger.info(f'Listing all supported files')
    # Get parquet files
    files = data[data['path'].str.endswith('.parquet')]['path']
    # Filter out files that we are sure have no header if possible
    # (we'll assume unlisted files do).
    header_data_file = Path('results/no_header.parquet')
    if header_data_file.exists:
        header_data = pd.read_parquet('results/no_header.parquet')
        no_header = header_data[~header_data['has_header']]['path']
        files = files[~files.isin(no_header)]
    # Note: we don't filter out files that contain the attribute because
    # we assume the search process is over for this attribute.
    # If it is not over, the user must pass the force option.
    logger.info(f'Got {len(files)} files to process')

    # If the attribute is in the dataset
    if (df['attribute'] == Worker.attribute).sum() > 0:
        if _args.force:
            # Remove lines with the attribute, and move on
            df = df[df['attribute'] != Worker.attribute]
        else:
            logger.info(
                f"Attribute {Worker.attribute!r} was found in the "
                f"previous results. Pass '--force' to overwrite them. "
                f"Exiting... "
            )
            exit(0)

    # Operate the pool
    pool = Pool(
        n_jobs=_args.n_jobs,
        worker_factory=Worker,
        logger=logger,
    )
    # Add all tasks to the pool
    for file in files:
        pool.put(file)
    pool.start()
    logger.info('Running !')

    final_results = result_consumer(
        logger=logger,
        pool=pool,
        final_results_factory=FinalResults,
    )

    # Merge the previously computed data with the new one
    final_df = pd.concat([final_results.df, df], ignore_index=True)
    # And write the final data
    final_df.to_csv(final_file, index=False)

    logger.info(
        f'Found {final_results.df.shape[0]} matching attributes this run. '
        f'Wrote results to {final_file!s}. '
    )
